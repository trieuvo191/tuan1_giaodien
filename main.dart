import 'dart:async';
import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'Example', home: MyHomePage());
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  String numberEddit = '';
  final numberEditingController = TextEditingController();
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize:
            Size(double.infinity, 230), // tang kich thuoc chieu cao của appBar
        // here the desired height
        child: AppBar(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30), //định dạng góc bo tròn với vuông
              bottomRight: Radius.circular(30),
            ),
          ),
          flexibleSpace: Padding(
            padding: EdgeInsets.all(80), //chia khoang cach tat ca deu bang 80
            child: Row(
              children: <Widget>[
                Column(
                  children: [
                    SvgPicture.asset('assets/a.svg'),
                    SvgPicture.asset('assets/b.svg'),
                  ],
                ),
                Column(
                  children: [
                    Text(
                      'Recovered',
                      style: TextStyle(fontSize: 30, color: Colors.white),
                    ),
                    Text(
                      'Health',
                      style: TextStyle(fontSize: 30, color: Colors.white),
                    ),
                  ],
                ),
              ],
            ),
          ),
          backgroundColor: Colors.blue[800],
        ),
      ),
      body: Container(
        child: Wrap(
          alignment: WrapAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(51, 30, 52, 18),
                ),
                Text(
                  'Enter Your Phone Number',
                  style: TextStyle(fontSize: 20, color: Colors.blue[900]),
                ),
                Wrap(
                  direction: Axis.vertical,
                  alignment: WrapAlignment.spaceBetween,
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(51, 10, 52, 16),
                ),
                RichText(
                  text: TextSpan(
                    text: 'We will send you the  ',
                    style: TextStyle(color: Colors.black, fontSize: 15),
                    children: <TextSpan>[
                      TextSpan(
                          text: '6 digit ',
                          style: TextStyle(
                              color: Colors.blueAccent, fontSize: 15)),
                      TextSpan(
                        text: 'verification',
                        style: TextStyle(color: Colors.black, fontSize: 15),
                      )
                    ],
                  ),
                ),
                Wrap(
                  direction: Axis.vertical,
                  alignment: WrapAlignment.spaceBetween,
                ),
                Text('code'),
                TextField(
                  maxLength: 7, //dinh dang cho chieu dai toi da la 7
                  controller: numberEditingController,
                  onChanged: ((Text) {
                    this.setState(
                      () {
                        numberEddit = Text;
                      },
                    );
                  }),
                  keyboardType: TextInputType.numberWithOptions(),
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.phone),
                    hintText: 'Your Phone Number',
                  ),
                ),
                ButtonTheme(
                  minWidth: 300.0,
                  height: 40.0,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(30),
                          bottomRight: Radius.circular(30),
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)),
                    ),
                    color: Colors.blue[800],
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (context) => SecondScreen(
                                  numberEddit:
                                      numberEddit, //nhận dữ liệu numberEddit truyền sang cho SecondScreen kế thừa.
                                )),
                      );
                    },
                    child: Text(
                      "NEXT",
                      style: TextStyle(fontSize: 25, color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class SecondScreen extends StatefulWidget {
  @override
  String numberEddit;
  SecondScreen({Key? key, required this.numberEddit}) : super(key: key);
  // ignore: library_private_types_in_public_api
  _SecondScreen createState() => _SecondScreen(numberEddit);
}

class _SecondScreen extends State<SecondScreen> {
  @override
  String numberEddit;
  String number = '(098)';
  _SecondScreen(this.numberEddit);
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(double.infinity, 230),
        // here the desired height
        child: AppBar(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
          ),
          flexibleSpace: Padding(
            padding: EdgeInsets.all(80),
            child: Row(
              children: <Widget>[
                Column(
                  children: [
                    SvgPicture.asset('assets/a.svg'),
                    SvgPicture.asset('assets/b.svg'),
                  ],
                ),
                Column(
                  children: [
                    Text(
                      'Recovered',
                      style: TextStyle(fontSize: 30, color: Colors.white),
                    ),
                    Text(
                      'Health',
                      style: TextStyle(
                        fontSize: 30,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          backgroundColor: Colors.blue[800],
        ),
      ),
      body: Container(
        child: Wrap(
          alignment: WrapAlignment.spaceBetween,
          children: [
            Column(children: [
              Padding(
                padding: EdgeInsets.fromLTRB(51, 30, 52, 18),
              ),
              Text(
                'Enter Your Phone Number',
                style: TextStyle(fontSize: 20, color: Colors.blue[900]),
              ),
              Wrap(
                direction: Axis.vertical,
                alignment: WrapAlignment.spaceBetween,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(51, 10, 52, 16),
              ),
              RichText(
                text: TextSpan(
                  text: 'We will send you the  ',
                  style: TextStyle(color: Colors.black, fontSize: 15),
                  children: <TextSpan>[
                    TextSpan(
                        text: '6 digit ',
                        style:
                            TextStyle(color: Colors.blueAccent, fontSize: 15)),
                    TextSpan(
                      text: 'verification',
                      style: TextStyle(color: Colors.black, fontSize: 15),
                    )
                  ],
                ),
              ),
              Wrap(
                direction: Axis.vertical,
                alignment: WrapAlignment.spaceBetween,
              ),
              Text('code'),
              DropdownButtonFormField<String>(
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(24, 10, 150, 0),
                    prefixIcon: Icon(Icons.phone),
                    suffixText: numberEddit,
                    suffixStyle:
                        TextStyle(color: Colors.blue[800], fontSize: 15),
                  ),
                  value: number,
                  onChanged: (String? Text) {
                    setState(() {
                      number = Text.toString();
                    });
                  },
                  items: <String>['(094)', '(098)', '(093)', '(034)', '(096)']
                      .map<DropdownMenuItem<String>>((String number) {
                    return DropdownMenuItem<String>(
                        value: number,
                        child: Text(
                          number,
                          style:
                              TextStyle(color: Colors.blue[800], fontSize: 15),
                        ));
                  }).toList()),
                  Padding(
                  padding: EdgeInsets.fromLTRB(51, 30, 52, 18),
                ),
              ButtonTheme(
                minWidth: 300.0,
                height: 40.0,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30),
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30)),
                  ),
                  color: Colors.blue[800],
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (context) => DetaildScreen(
                                number: number,
                                numberEddit: numberEddit,
                              )),
                    );
                  },
                  child: Text(
                    "NEXT",
                    style: TextStyle(fontSize: 25, color: Colors.white),
                  ),
                ),
              ),
            ]),
          ],
        ),
      ),
    );
  }
}

class DetaildScreen extends StatefulWidget {
  @override
  String number;
  String numberEddit;
  DetaildScreen({Key? key, required this.number, required this.numberEddit})
      : super(key: key);
  _DetailScreen createState() => _DetailScreen(number, numberEddit);
}

class _DetailScreen extends State<DetaildScreen> {
  @override
  String number;
  String numberEddit;
  _DetailScreen(this.number, this.numberEddit);
  dynamic counter;
  int _counter1 = 60;

  Widget build(BuildContext context) {
    late Timer _timer;
    void Start() {
      _counter1 = 60;
      _timer = Timer.periodic(Duration(seconds: 1), (timer) {
        if (_counter1 > 0) {
          this.setState(() {
            _counter1--;
            print(_counter1);
          });
        } else {
          timer.cancel();
        }
      });
    }

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(double.infinity, 230),
        // here the desired height
        child: AppBar(
          automaticallyImplyLeading: false,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
          ),
          flexibleSpace: Padding(
            padding: EdgeInsets.all(80),
            child: Row(
              children: <Widget>[
                Column(
                  children: [
                    SvgPicture.asset('assets/a.svg'),
                    SvgPicture.asset('assets/b.svg'),
                  ],
                ),
                Column(
                  children: [
                    Text(
                      'Recovered',
                      style: TextStyle(fontSize: 30, color: Colors.white),
                    ),
                    Text(
                      'Health',
                      style: TextStyle(fontSize: 30, color: Colors.white),
                    ),
                  ],
                ),
              ],
            ),
          ),
          backgroundColor: Colors.blue[800],
        ),
      ),
      body: Container(
        child: Wrap(
          alignment: WrapAlignment.spaceBetween,
          children: <Widget>[
            Column(
              children: [
                Row(
                  children: [
                    IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: Icon(
                          Icons.arrow_back_ios_outlined,
                          color: Colors.blue[800],
                        )),
                    Text(
                      'BACK',
                      style: TextStyle(
                        color: Colors.blue[800],
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(51, 30, 52, 18),
                ),
                Text(
                  'OTP Verification',
                  style: TextStyle(fontSize: 20, color: Colors.blue[900]),
                ),
                Wrap(
                  direction: Axis.vertical,
                  alignment: WrapAlignment.spaceBetween,
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(51, 10, 52, 16),
                ),
                Text('A verification codes  has been sent'),
                RichText(
                  text: TextSpan(
                    text: 'to ',
                    style: TextStyle(color: Colors.black, fontSize: 15),
                    children: <TextSpan>[
                      TextSpan(
                        text: number,
                        style: TextStyle(color: Colors.blue, fontSize: 15),
                      ),
                      TextSpan(
                        text: ' ',
                        style: TextStyle(color: Colors.blue, fontSize: 15),
                      ),
                      TextSpan(
                        text: numberEddit,
                        style: TextStyle(color: Colors.blue, fontSize: 15),
                      ),
                    ],
                  ),
                ),
                Wrap(
                  direction: Axis.vertical,
                  alignment: WrapAlignment.spaceBetween,
                ),
                Padding(padding: EdgeInsets.all(30)),
                Wrap(
                  alignment: WrapAlignment.center,
                  children: <Widget>[
                    MyContainer(),
                    MyContainer(),
                    MyContainer(),
                    MyContainer(),
                    MyContainer(),
                    MyContainer(),
                  ],
                ),
                ButtonTheme(
                  minWidth: 300.0,
                  height: 30.0,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(30),
                            bottomRight: Radius.circular(30),
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30))),
                    color: Colors.blue[800],
                    onPressed: () {},
                    child: Text(
                      "VERIFY & CONTINUE",
                      style: TextStyle(fontSize: 25, color: Colors.white),
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.all(20)),
                Text(
                  'Send again OTP',
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ),
                Text(
                  '00:$_counter1',
                  style: TextStyle(color: Colors.blue[500], fontSize: 20),
                ),
                ButtonTheme(
                  minWidth: 300.0,
                  height: 40.0,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(30),
                            bottomRight: Radius.circular(30),
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30))),
                    color: Colors.white,
                    onPressed: () {
                      Start();
                      // setState(() {
                      //   _counter1--;
                      // });
                      // while (_counter1 == 0) {
                      //   Text('Wanto Resend?');
                      // }
                    },
                    child: Text(
                      "Resent Code",
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.blue,
                          decoration: TextDecoration.underline),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

// class ArgumentScreen {
//   String _number1;
//   int _counter;
//   ArgumentScreen(this._number1, this._counter);
// }

class MyContainer extends StatelessWidget {
  const MyContainer({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(7),
      width: 50,
      height: 50,
      color: Colors.white,
      child: TextField(
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 25, color: Colors.black),
        keyboardType: TextInputType.numberWithOptions(),
        decoration: InputDecoration(hintText: '-'),
      ),
    );
  }
}
